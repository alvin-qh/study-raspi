# Raspberry Pi Getting Started

## Setup Respberry Pi

### 1. Download image file

In [Raspberry Pi download page](https://www.raspberrypi.org/downloads/raspbian/), download image file. (eg: `2018-10-09-raspbian-stretch-lite.zip`)

Uncompress `.zip` file, get `.img` file

```bash
$ unzip ./2018-10-09-raspbian-stretch-lite.zip
```

### 2. Burn image on SD card

- Find name of sdcard filesystem

  ```bash
  $ df -h
  
  # macOS
  Filesystem     Size   Used  Avail Capacity iused    ifree %iused  Mounted on
  ...	 # other filesystem
  /dev/disk2s1   43Mi   22Mi   21Mi    51%       0        0  100%   /Volumes/boot
  
  # Linux
  Filesystem    size   Used Avail Use% Mounted on
  ... # other filesystem
  /dev/sdb1     512M   4K   512M    1% /media/LABLE1
  /dev/sdb2     6.8G   4K   6.8G    1% /media/LABLE2
  ```

- Unmount sdcard filesystem

  ```bash
  # macOS
  $ sudo diskutil umount /dev/disk2s1
  
  # Linux
  $ sudo umount /dev/sdb1 /dev/sdb2
  ```

- Download `SDCard Formatter` from [Home](https://www.sdcard.org/downloads/formatter_4/eula_mac/index.html), format sdcard with this tool, or just use shell command

  ```bash
  # macOS
  $ sudo diskutil eraseDisk FAT32 RASPBIAN MBRFormat /dev/disk2
  
  # linux
  $ sudo fdisk /dev/sdb
  
  Command (m for help): m		# for help
  Command action
     a   toggle a bootable flag
     b   edit bsd disklabel
     c   toggle the dos compatibility flag
     d   delete a partition
     l   list known partition types
     m   print this menu
     n   add a new partition
     o   create a new empty DOS partition table
     p   print the partition table
     q   quit without saving changes
     s   create a new empty Sun disklabel
     t   change a partition's system id
     u   change display/entry units
     v   verify the partition table
     w   write table to disk and exit
     x   extra functionality (experts only)
     
  Command (m for help): p		# list filesystem
  Disk /dev/sdb: 7948 MB, 7948206080 bytes
  245 heads, 62 sectors/track, 1021 cylinders, total 15523840 sectors
  Units = sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disk identifier: 0x00000000
  
     Device Boot      Start         End      Blocks   Id  System
  /dev/sdb1   *        2048     1048576      523264+   c  W95 FAT32 (LBA)
  /dev/sdb2         1048577    15523839     7237631+  83  Linux
  
  Command (m for help): d	 	# delete partition 1
  Partition number (1-4): 1
  
  Command (m for help): d		# delete partition 2
  Selected partition 2
  
  Command (m for help): x		# enter expert command mode
  Expert command (m for help): n	# list expert commands
  Command action
     b   move beginning of data in a partition
     c   change number of cylinders
     d   print the raw data in the partition table
     e   list extended partitions
     f   fix partition order
     g   create an IRIX (SGI) partition table
     h   change number of heads
     i   change the disk identifier
     m   print this menu
     p   print the partition table
     q   quit without saving changes
     r   return to main menu
     s   change number of sectors/track
     v   verify the partition table
     w   write table to disk and exit
  
  Expert command (m for help): h		# change number of heads to 255
  Number of heads (1-256, default 245): 255
  
  Expert command (m for help): s		# set sectors to 63
  Number of sectors (1-63, default 62): 63
  
  Expert command (m for help): c		# set cylinders to 966
  # cylinder = sdcard size(in bytes) / heads / sectors / 512
  # 7948206080 / 255 /63 / 512 = 966.3
  Number of cylinders (1-1048576, default 1021): 966
  
  Expert command (m for help): r		# return normal mode
  Command (m for help): p				# show changed sdcard information
  Disk /dev/sdb: 7948 MB, 7948206080 bytes
  255 heads, 63 sectors/track, 966 cylinders, total 15523840 sectors
  Units = sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 512 bytes
  I/O size (minimum/optimal): 512 bytes / 512 bytes
  Disk identifier: 0x00000000
  
     Device Boot      Start         End      Blocks   Id  System
  
  Command (m for help): n		# create new partition
  Partition type:
     p   primary (0 primary, 0 extended, 4 free)
     e   extended
  Select (default p): p		# select primary partition type
  Partition number (1-4, default 1): 1	# create first partition
  First sector (2048-15523839, default 2048): 2048	# sector start
  Last sector, +sectors or +size{K,M,G} (2048-15523839, default 15523839): 15523839 # sector end 
  
  Command (m for help): t		# change partition type
  Selected partition 1		# select partition number
  Hex code (type L to list codes): c		# change type to FAT32
  Changed system type of partition 1 to c (W95 FAT32 (LBA))
  
  Command (m for help): a		# activate partition 1
  Partition number (1-4): 1
  
  Command (m for help): w		# save changes
  The partition table has been altered!
  
  Calling ioctl() to re-read partition table.
  
  WARNING: If you have created or modified any DOS 6.x
  partitions, please see the fdisk manual page for additional
  information.
  Syncing disks.
  
  # format partition
  $ sudo mkfs.msdos -F32 /dev/sdb1 -n LABEl1
  ```

- Burn `.img` file to sdcard

  ```bash
  $ sudo dd bs=1m if=./2018-10-09-raspbian-stretch-lite.img of=/dev/rdisk2  
  ```

### 3. Config before first boot

- Open sdcard into PC, and enter `boot` partition (volume name is `boot`)

- Create `wpa_supplicant.conf` file

  ```bash
  $ touch wpa_supplicant.conf
  ```

- Write WIFI config into `wpa_supplicant.conf` file, add the following content

  ```bash
  country=CN
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  
  network={
      ssid="WIFI SSID"
      psk="WIFI password"
      key_mgmt=WPA-PSK
      priority=1		# first network interface
  #   scan_ssid=1		# if SSID is hidden
  }
  ```

- Enable `ssh`，create `ssh` file in same area

  ```bash
  $ touch ssh
  ```

- Insert sdcard in Raspberry Pi, boot the device, and find device IP address (maybe `192.168.0.10`), then login by `ssh` with user `pi` and password `raspberry`

  ```bash
  $ ssh pi@192.168.0.10
  
  pi@192.168.0.10's password: raspberry
  ```

- Make sure IP address is static

  - Edit `/etc/dhcpcd.conf`file, add the following contents at end of:

    ```bash
    interface eth0
    static ip_address=192.168.0.10/24
    static routers=192.168.0.1
    static domain_name_servers=192.168.0.1 8.8.8.8
    
    interface wlan0
    static ip_address=192.168.0.200/24
    static routers=192.168.0.1
    static domain_name_servers=192.168.0.1 8.8.8.8
    ```

  - Edit `/etc/wpa_supplicant/wpa_supplicant.conf` file, add the config options:

    ```bash
    country=CN
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    
    network={
        ssid="WIFI SSID"
        psk="WIFI password"
        key_mgmt=WPA-PSK
        priority=1		# first network interface
    #   scan_ssid=1		# if SSID is hidden
    }
    ```



#### 4. Enable/Disable wlan

For wlan function

```bash
$ ip link set wlan0 up
$ ip link set wlan0 down
```

For wlan device

```bash
$ sudo rfkill block 0
$ sudo rfkill unblock 0
```



### 5. Create users and setup ssh

- Change `pi` user password

  ```bash
  $ sudo passwd pi
  ```

- Create other user (eg: alvin)

  ```bash
  $ useradd -d /home/alvin -m -s /bin/bash -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi alvin
  
  $ sudo passwd alvin
  $ sudo su alvin		# change current user
  $ cd 				# goto home
  ```

- Create ssh keys (not necessary)

  ```bash
  $ mkdir ~/.ssh
  $ chmod 700 ~/.ssh
  $ cd ~/.ssh
  
  $ ssh-keygen -t rsa -P '' -C alvin
  ```

- Copy `public key` on other device (witch want to login this raspberry pi) 

  ```bash
  $ echo (public key) > authorized_keys
  $ chomod 600 authorized_keys
  ```

#### 6. Config use raspi-config tool

```bash
$ sudo raspi-config
```

#### 7. Setup source lists

- Edit `/etc/apt/sources.list`

  ```bsh
  #deb http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi
  deb http://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ stretch main non-free contrib
  
  # Uncomment line below then 'apt-get update' to enable 'apt-get source'
  # deb-src http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi
  ```

  Update 

  ```bash
  $ sudo apt update && sudo apt upgrade
  ```
